const sumar = require("../index");
const assert = require("assert");

describe("Probar la suma de dos numeros", ()=>{

    //Afirmar que 5 + 7 es 12
    it("5 + 7 es 12", ()=>{
        assert.equal(12, sumar(5,7));
    });
    
    //Afirmamos que 5 + 7 no es 57
    it("5 + 7 no es 57", ()=>{
        assert.notEqual("57", sumar(5,7));
    });

});

